## Documents

  1. [Installation](https://bitbucket.org/KinGLeGenD/filemanager/src/master/doc/installation.md)
  1. [Integration](https://bitbucket.org/KinGLeGenD/filemanager/src/master/doc/integration.md)
  1. [Config](https://bitbucket.org/KinGLeGenD/filemanager/src/master/doc/config.md)
  1. [Customization](https://bitbucket.org/KinGLeGenD/filemanager/src/master/doc/customization.md)
  1. [Events](https://bitbucket.org/KinGLeGenD/filemanager/src/master/doc/events.md)
  1. [**Upgrade**](https://bitbucket.org/KinGLeGenD/filemanager/src/master/doc/upgrade.md)

## Upgrade guide
  * `composer update mediapress/filemanager`
  * `php artisan vendor:publish --tag=lfm_view --force`
  * `php artisan vendor:publish --tag=lfm_config --force` (IMPORTANT: please backup your own `config/lfm.php` first)
