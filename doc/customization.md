## Documents

  1. [Installation](https://bitbucket.org/KinGLeGenD/filemanager/src/master/doc/installation.md)
  1. [Integration](https://bitbucket.org/KinGLeGenD/filemanager/src/master/doc/integration.md)
  1. [Config](https://bitbucket.org/KinGLeGenD/filemanager/src/master/doc/config.md)
  1. [**Customization**](https://bitbucket.org/KinGLeGenD/filemanager/src/master/doc/customization.md)
  1. [Events](https://bitbucket.org/KinGLeGenD/filemanager/src/master/doc/events.md)
  1. [Upgrade](https://bitbucket.org/KinGLeGenD/filemanager/src/master/doc/upgrade.md)

## Customization

Feel free to customize the routes and views if your need.

### Routes

1. Copy the routes in /vendor/mediapress/filemanager/src/routes.php

1. Make sure urls below is correspond to your route :

  CKEditor
    ```javascript
        <script>
            CKEDITOR.replace( 'editor', {
                filebrowserImageBrowseUrl: '/your-custom-route?type=Images',
                filebrowserBrowseUrl: '/your-custom-route?type=Files',
            });
        </script>
    ```
    
  And be sure to include the `?type=Images` or `?type=Files` parameter.

  TinyMCE
    ```javascript
        ...
        var cmsURL = editor_config.path_absolute + 'your-custom-route?field_name='+field_name+'&lang='+ tinymce.settings.language;
        if (type == 'image') {
          cmsURL = cmsURL + "&type=Images";
        } else {
          cmsURL = cmsURL + "&type=Files";
        }
        ...
    ```

### Views

1. Copy the views from /vendor/filemanager/laravel-filemanager/src/views/ :

    ```bash
    php artisan vendor:publish --tag=lfm_view
    ```

### Translations

1. Copy `vendor/mediapress/filemanager/src/lang/en` to `/resources/lang/vendor/filemanager/<YOUR LANGUAGE>/lfm.php`
2. Change the file according your preferences