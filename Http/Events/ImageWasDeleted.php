<?php

namespace Mediapress\FileManager\Http\Events;

class ImageWasDeleted
{
    private $path;

    public function __construct($path)
    {
        $this->path = $path;
    }

    /**
     * @return string
     */
    public function path()
    {
        return $this->path;
    }

}
