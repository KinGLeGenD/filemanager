<?php
namespace Mediapress\FileManager\Http\Controllers;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Mediapress\Models\Gallery;
use Mediapress\Models\Image;

/**
 * Class ItemsController
 * @package Mediapress\FileManager\Http\Controllers
 */
class ItemsController extends LfmController
{

    private $path;

    /**
     * Get the images to load for a selected folder
     *
     * @return mixed
     */
    public function getItems()
    {
        $type = Input::get('type');
        $view = $this->getView();
        $this->path = parent::getPath();

        $files = File::files($this->path);

        $file_info = $this->getFileInfos($files, $type);
        $directories = parent::getDirectories($this->path);
        $thumb_url = parent::getUrl('thumb');

        return view($view, compact('type', 'file_info', 'directories', 'thumb_url'));
    }


    private function getFileInfos($files, $type = 'Images')
    {
        $file_info = [];

        foreach ($files as $key => $file)
        {
            $file_name = parent::getFileName($file)['short'];
            $file_created = filemtime($file);
            $file_size = number_format((File::size($file) / 1024), 2, ".", "");

            if ($file_size > 1024)
            {
                $file_size = number_format(($file_size / 1024), 2, ".", "") . " Mb";
            }
            else
            {
                $file_size = $file_size . " Kb";
            }

            if ($type === 'Images')
            {
                $file_type = File::mimeType($file);
                $fileInfo = new \SplFileInfo($file);
                
                //Thumb yoksa oluşturur
                if (!File::exists($fileInfo->getPath() . DIRECTORY_SEPARATOR . Config::get('lfm.thumb_folder_name') . DIRECTORY_SEPARATOR . $fileInfo->getFilename()))
                {
                    app(UploadController::class)->makeThumb($fileInfo->getPath() . DIRECTORY_SEPARATOR, $fileInfo->getFilename());
                }
                    $icon = '';

                $gallery = Gallery::select("id")->where("path", $this->path . DIRECTORY_SEPARATOR)->first();
                $file_id = 0;
                if ($gallery)
                {
                    $file = Image::select("id")->where("gallery_id", $gallery->id)->where("file_name", $file_name)->first();
                    if ($file)
                    {
                        $file_id = $file->id;
                    }
                }
            }
            else
            {
                $extension = strtolower(File::extension($file_name));

                $icon_array = Config::get('lfm.file_icon_array');
                $type_array = Config::get('lfm.file_type_array');

                if (array_key_exists($extension, $icon_array))
                {
                    $icon = $icon_array[$extension];
                    $file_type = $type_array[$extension];
                }
                else
                {
                    $icon = "fa-file";
                    $file_type = "File";
                }
            }

            $file_info[$key] = [
                'id' => $file_id,
                'name' => $file_name,
                'size' => $file_size,
                'created' => $file_created,
                'type' => $file_type,
                'icon' => $icon,
            ];
        }

        return $file_info;
    }


    private function getView()
    {
        if (Input::get('show_list') == 1)
        {
            return 'filemanager::list-view';
        }
        else
        {
            return 'filemanager::grid-view';
        }
    }
}
