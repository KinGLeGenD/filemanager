<?php
namespace Mediapress\FileManager\Http\Controllers;

use Mediapress\Models\Gallery;
use Mediapress\Models\Image;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Mediapress\FileManager\Http\Events\ImageWasRenamed;
use Mediapress\FileManager\Http\Events\FolderWasRenamed;

/**
 * Class RenameController
 * @package Mediapress\FileManager\Http\Controllers
 */
class RenameController extends LfmController
{

    /**
     * @return string
     */
    public function getRename()
    {
        $old_name = Input::get('file');
        $new_name = trim(Input::get('new_name'));

        $file_path = parent::getPath('directory');
        $thumb_path = parent::getPath('thumb');

        $old_file = $file_path . $old_name;

        if (! File::isDirectory($old_file))
        {
            $extension = File::extension($old_file);
            $new_name = str_replace('.' . $extension, '', $new_name) . '.' . $extension;
        }

        $new_file = $file_path . $new_name;

        if (Config::get('lfm.alphanumeric_directory') && preg_match('/[^\w-]/i', $new_name))
        {
            return trans('filemanager::lfm.error-folder-alnum');
        } elseif (File::exists($new_file))
        {
            return trans('filemanager::lfm.error-rename');
        }

        if (File::isDirectory($old_file))
        {
            if(Config::get("lfm.folder_rename") == false){
                return trans('filemanager::lfm.error-rename');
            }
            File::move($old_file, $new_file);
            Gallery::where("path", $file_path)->where("folder_name", $old_name)->update(["folder_name" => $new_name]);
            Event::fire(new FolderWasRenamed($old_file, $new_file));
            return 'OK';
        }

        File::move($old_file, $new_file);

        if ('Images' === $this->file_type)
        {
            File::move($thumb_path . $old_name, $thumb_path . $new_name);
            $gallery = Gallery::select("id")->where("path", $file_path)->first();
            $result = Image::where("gallery_id", $gallery->id)->where("file_name", $old_name)->update(["file_name" => $new_name]);
        }

        Event::fire(new ImageWasRenamed($old_file, $new_file));

        return 'OK';
    }
}
