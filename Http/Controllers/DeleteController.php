<?php
namespace Mediapress\FileManager\Http\Controllers;

use Mediapress\Models\Gallery;
use Mediapress\Models\Image;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Mediapress\FileManager\Http\Events\ImageWasDeleted;

/**
 * Class CropController
 * @package Mediapress\FileManager\Http\Controllers
 */
class DeleteController extends LfmController
{

    /**
     * Delete image and associated thumbnail
     *
     * @return mixed
     */
    public function getDelete()
    {
        $name_to_delete = Input::get('items');

        $file_path = parent::getPath('directory');

        $file_to_delete = $file_path . $name_to_delete;
        $thumb_to_delete = parent::getPath('thumb') . $name_to_delete;

        //Silinen Resmin yada Klasörün Gideceği Hedef Belirleniyor
        $dest_path = storage_path("trash" . DIRECTORY_SEPARATOR . "gallery" . DIRECTORY_SEPARATOR . date("Y-m-d") . DIRECTORY_SEPARATOR);
        $file_new_name = time() . "-" . $name_to_delete;
        //Silinenler klasörü yoksa oluşturulması sağlanıyor

        if (! File::exists($dest_path))
        {
            File::makeDirectory($dest_path, 777, true);
        }

        if (! File::exists($file_to_delete))
        {
            return $file_to_delete . ' not found!';
        }


        if (File::isDirectory($file_to_delete))
        {
            if (sizeof(File::files($file_to_delete)) != 0)
            {
                return trans('filemanager::lfm.error-delete');
            }

            //Silinen Klasör Taşınıyor
            File::moveDirectory($file_to_delete, $dest_path . $file_new_name);
            $gallery = Gallery::where("path", $file_path);
            $gallery->update(["delete_name" => $file_new_name]);
            $gallery->delete();
//            File::deleteDirectory($file_to_delete);

            return 'OK';
        }


        #region Veritabanından Dosya Siliniyor
        $gallery = Gallery::select("id")->where("path", $file_path)->first();

        if ($gallery)
        {
            $image = Image::where("gallery_id", $gallery->id)->where("file_name", $name_to_delete);
            $image->update(["delete_name" => $file_new_name]);
            $image->delete();
            File::move($file_to_delete, $dest_path . $file_new_name);
            Event::fire(new ImageWasDeleted($file_to_delete));

            if ('Images' === $this->file_type)
            {
                File::delete($thumb_to_delete);
            }

            return 'OK';

        }
        #endregion

        return "Can't File Delete";
    }

}
