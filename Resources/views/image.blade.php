
<div class="modal-body">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
    <img src="{{$src}}" alt="" />
</div>
<style>
    .modal-body img{
        max-width: 100%;
    }
    .modal-body,.modal-header{
       padding: 0;
    }
    .modal-body button{
        position: absolute;
        right: -40px;
        top: 0px;
        color: white;
        padding: 10px 14px;
        background: black;
        opacity: 1;
    }
</style>