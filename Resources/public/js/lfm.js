var selectLanguage = 0;
(function ($)
{
    $.fn.filemanager = function (type, language, selector)
    {
        selectLanguage = language;
        type = type || 'image';

        if (type === 'image' || type ===
            'images')
        {
            type = 'Images';
        }
        else
        {
            type = 'Files';
        }
        if (selector)
        {
            $('body').delegate(selector, 'click', function (e)
            {
                localStorage.setItem('target_input', $(this).data('input'));
                localStorage.setItem('target_preview', $(this).data('preview'));
                localStorage.setItem('return_type', $(this).data('type'));
                localStorage.setItem('unique_pic', $(this).data('unique'));

                window.open('/mp-admin/file-manager?type=' + type, 'FileManager',
                    'width=900,height=600'
                );
                return false;
            });
        } else
        {
            this.on('click', function (e)
            {
                localStorage.setItem('return_type', this.data('type'));
                localStorage.setItem('target_input', this.data('input'));
                localStorage.setItem('target_preview', this.data('preview'));
                localStorage.setItem('unique_pic', this.data('unique'));
                window.open('/mp-admin/file-manager?type=' +
                    type, 'FileManager',
                    'width=900,height=600'
                );
                return false;
            });
        }
    }
})(jQuery);
var list = [];
var imageId = 0;
function SetUrl(url, id)
{
    // var image = url.substr(url.lastIndexOf("photos") + 6);

    if (jQuery.inArray(url, list[selectLanguage]) == - 1)
    {
        if (list[selectLanguage] == undefined)
        {
            list[selectLanguage] = [];
        }
        //set the value of the desired input to image image
        var target_input = $('body').find('.' + localStorage.getItem('target_input'));

        var target_inputurl = $('body').find('#' + localStorage.getItem('target_input') + 'url');

        if (localStorage.getItem('return_type') == 'id')
        {
            target_input.val(id + '#' + url);
        } else
        {
            target_input.val(url);
            target_inputurl.val(url);
        }
        if (localStorage.getItem('target_input'))
        {

            var selector = $("input[type=hidden][name='" + localStorage.getItem("target_input") + "']");
            if (localStorage.getItem('return_type') == 'id')
            {
                selector.val(id).prev().removeAttr("name");
            } else
            {
                selector.val(url).prev().removeAttr("name");
            }
            selector.parent().parent().parent().removeClass("fileinput-new").addClass("fileinput-exists");
        }

        if (localStorage.getItem('target_preview'))
        {
            $("div[id='" + localStorage.getItem("target_preview") + "']").html('<img src="' + url + '" class="img-responsive">');

            // $(localStorage.getItem('target_preview')).popover({
            //     content : '<img src="' + url + '" class="img-responsive">',
            //     title : 'Preview', placement : "top", html : true
            // }).blur(function ()
            // {
            //     $(this).popover('hide');
            // });
        }

        if (localStorage.getItem('unique_pic') == true)
        {
            list[selectLanguage].push(url, id);
        }

        if (id)
            imageId = id;
    }
    else
    {
        swal(
            'Hata!',
            'Resim önceden seçilmiş!',
            'error'
        )
    }
}