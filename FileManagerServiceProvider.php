<?php

namespace Mediapress\FileManager;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Config;

class FileManagerServiceProvider extends ServiceProvider
{
    public function boot()
    {
        if (Config::get('lfm.use_package_routes'))
            include __DIR__ . '/Http/routes.php';
        $this->loadTranslationsFrom(__DIR__.'/Resources/lang', 'filemanager');
        $this->loadViewsFrom(__DIR__.'/Resources/views', 'filemanager');
        $this->publishes([
            __DIR__ . '/config/lfm.php' => base_path('config/lfm.php'),
        ], 'lfm_config');
        $this->publishes([
            __DIR__.'/Resources/public' => public_path('vendor/filemanager'),
        ], 'lfm_public');
        $this->publishes([
            __DIR__.'/Resources/views'  => base_path('resources/views/vendor/filemanager'),
        ], 'lfm_view');
    }
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('filemanager',function ()
         {
             return true;
         });
    }
}